define([
  'angular',
  'app',
  'underscore'
],
function (angular, app, _, $rootScope) {
  'use strict';

  angular
    .module('kibana.directives')
    .directive('parsestorelinks', function($compile, $filter, $rootScope) {
      return {
        scope: {
            value: '@',
            key: '@',
            panel: '@'
        },
        restrict: 'E',
        link: function(scope, elem, attrs) {

          scope.applyFilter = function(field, store){
              $rootScope.$broadcast('onFilterStores', [field, store]);
          };

          attrs.$observe('key', function(k) {
            if (k) {
              let template = scope.value;

              // determine if the value is an array
              try{
                const isJson = JSON.parse(scope.value);
                if(Array.isArray(isJson)){
                    template = isJson.map(function (e) {
                      return e;
                    }).join(',');
                }
              }catch(e){

              }

              template = $filter('noXml')(template);
              template = $filter('urlLink')(template);
              template = $filter('stringify')(template);

              switch(k){
                case 'all_associated_stores':
                case 'primary_store':
                case 'all_stores':
                  // get the array or store names
                  template = template.split(',').map(function (store) {
                    return '<a ng-click="applyFilter(\''+k+'\',\''+store+'\')">'+store.trim()+'</a>';
                  }).join('<br/>');

                  break;
                default:
                  template = $filter('onFormatLinks')(template);
                  template = $filter('commaToLineBreak')(template);
                  break;
              }

              template = '<span>'+template+'</span>';

              elem.html($compile(angular.element(template))(scope));
            }
          });
        }
      };
    });
});
