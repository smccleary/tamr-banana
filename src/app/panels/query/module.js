/*

  ## query

  ### Parameters
  * query ::  A string or an array of querys. String if multi is off, array if it is on
              This should be fixed, it should always be an array even if its only
              one element
              */
define([
  'angular',
  'app',
  'underscore',
  'css!./query.css'
  ], function (angular, app, _) {
    'use strict';

    var module = angular.module('kibana.panels.query', []);
    app.useModule(module);

    module.controller('query', function ($scope, querySrv, $rootScope, $http) {
      $scope.panelMeta = {
        modals: [{
          description: "Inspect",
          icon: "icon-info-sign",
          partial: "app/partials/inspector.html",
          show: true
        }],
        status: "Stable",
        description: "Provide a search bar for free-form queries. You almost certainly want one of these somewhere prominent on your dashboard."
      };

    // set and populate defaults
    var _d = {
      query: "*:*",
      pinned: true,
      history: [],
      tags: [],
      spyable: true,
      remember: 10, // max: 100, angular strap can't take a variable for items param
    };
    _.defaults($scope.panel, _d);

    $scope.querySrv = querySrv;

    $scope.init = function () {
      //
    };

    // get suggestion from solr
    $scope.onGetSuggestionsFromSolr = function(term, collection, host){
      // call out to solr to get suggestions and process
      return $http({
        method: 'GET',
        url: host+'/solr/'+collection+'/suggest?suggest=true&suggest.dictionary=primary_store&suggest.dictionary=all_associated_stores&suggest.dictionary=all_stores&wt=json&suggest.q='+term+'&suggest.build=true'
      }).then(function successCallback(response) {

        const tree = response.data.suggest,
              stores = Object.keys(tree),
              allSuggestions = [];

        // iterate the stores to get the suggestions
        for(var i in stores){
          const store = stores[i],
                suggestions = tree[store][term]['suggestions'];
          
          // pipe each suggestion to the stack
          for(var si in suggestions){
            const entry = suggestions[si];

            // prepend the field and add the query tag
            allSuggestions.push(store + ':' + entry.term);
          }
        }

        // return the suggestions 
        return allSuggestions;
      }, function errorCallback(response) {
        console.error('onGetSuggestionsFromSolr', response);
      });
    };

    // listen for the status of the term search module and set the loading status accordingly
    $scope.status = '&nbsp;';

    $scope.$on('onLoadingTrue', function (event, args) {
      $scope.panelMeta.loading = true;
      $scope.status = 'Updating search table results, please wait...';
    });

    $scope.$on('onLoadingFalse', function (event, args) {
      $scope.panelMeta.loading = false;
      $scope.status = '&nbsp;';
    });

    // watch the input and update the typeahead options accordingly
    $scope.$watch('querySrv.list', function (newValues, oldValues, scope) {
      try {
        let query = newValues[1].query;
        if (query && query.length) {
          // get the fields from the schema
          const fields = $scope.fields.list;

          // iterate the fields and return as typeahead options
          $scope.panel.tags = fields.filter(function (tag) {
            // exclude these tags as they do not return a viable search
            return ['Index', '_root_', '_version_', 'weightf', 'stage', 'cjp_id', 'proprietary',
            'ser', 'folder', 'data_type', 'clearance_date', 'job_id', 'project_id', 'extension',
            'all_associated_stores', 'primary_store', 'all_stores'].indexOf(tag) === -1;
          }).map(function (tag) {
            // remove any field prefixes if present and get the query string only
            if (query.indexOf(':') >= 0) query = query.split(':')[1].trim();
            return tag + ':' + query;
          });

          // append the results to the tags from the solr suggester query
          $scope.onGetSuggestionsFromSolr(query, 'afseoTags1', 'http://localhost:8080').then(function(suggestions){
            // walk the results and append to the tags
            for(var i in suggestions){
              $scope.panel.tags.push(suggestions[i]);
            }          
          });

          // add the global search option to the top of the typeahead
          $scope.panel.tags.unshift('*:' + query);
        }
      } catch (e) {
        //
      }
    }, true);

    $scope.reset = function () {
      const query = _d.query;
      try {
        $scope.querySrv.list[Object.keys($scope.querySrv.list).length - 1].query = query;
      } catch (e) {
        $scope.querySrv.list[1].query = query;
      }
      $rootScope.$broadcast('refresh');
    };

    $scope.refresh = function () {
      update_history(_.pluck($scope.querySrv.list, 'query'));
      $rootScope.$broadcast('refresh');
    };

    $scope.render = function () {
      $rootScope.$broadcast('render');
    };

    $scope.toggle_pin = function (id) {
      querySrv.list[id].pin = querySrv.list[id].pin ? false : true;
    };

    $scope.close_edit = function () {
      $scope.refresh();
    };

    var update_history = function (query) {
      if ($scope.panel.remember > 0) {
        // do not save the colon delimited queries to history. they may not be
        // used as we are overriding the type-ahead
        if (query.indexOf(':') === -1) {
          $scope.panel.history = _.union(query.reverse(), $scope.panel.history);
          var _length = $scope.panel.history.length;
          if (_length > $scope.panel.remember) {
            $scope.panel.history = $scope.panel.history.slice(0, $scope.panel.remember);
          }
        }
      }
    };

    $scope.init();
  });
});
